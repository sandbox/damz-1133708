<?php

class DatabaseTasks_failover {
  function __construct() {
    $connection_info = Database::getConnectionInfo();
    $driver = isset($connection_info['default']['parent_driver']) ? $connection_info['default']['parent_driver'] : 'mysql';
    $this->proxy = db_installer_object($driver);
  }

  function __call($method, $arguments) {
    if (isset($this->proxy)) {
      return call_user_func_array(array($this->proxy, $method), $arguments);
    }
  }

  public function name() {
    return st('Failover connection');
  }

  public function getFormOptions($database) {
    $form['help'] = array(
      '#markup' => st('This type of connection cannot be configured from the UI. See README.txt for installation instructions.')
    );
    return $form;
  }
}

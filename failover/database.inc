<?php

/**
 * Poor-man failover support.
 *
 * This special database engine implementation handles failover between a set
 * of database connections. The current master is centrally stored in a
 * pluggeable failover agent.
 *
 * Any type of database engine should work transparently, but this was designed
 * with MySQL double-master deployment strategy, where no better failover
 * mechanism is available.
 *
 * Using this in any kind of serious production context is strongly discouraged.
 * The application (here Drupal) is the worst place to put the failover logic,
 * which is best handled by the platform. If your hosting company recommended
 * you to use this, consider switching to a more professional provider.
 *
 * To use this engine, copy the whole "failover" directory to includes/database.
 * This engine is used as any other engine, by declaring the connection in
 * the settings.php file:
 *
 * @code
 * $databases['default']['default'] = array(
 *   'driver' => 'failover',
 *
 *   // The name of the driver that will be used to establish the connections.
 *   'parent_driver' => 'mysql',
 *
 *   // The servers. Each server is an associative array that can override any of
 *   // the values defined at the top-level.
 *   'servers' => array(
 *     array(
 *       'host' => 'master-1.local',
 *     ),
 *     array(
 *       'host' => 'master-2.local',
 *       'port' => '3304',
 *     ),
 *   ),
 *
 *   // The agent. Used to store the current master server.
 *   'agent' => array('DatabaseFailoverAgent_fs', array('sites/default/.master')),
 *
 *   // Other standard connection keys.
 *   'database' => 'mydb',
 *   'username' => 'myuser',
 *   'password' => 'mypass',
 *   'prefix' => '',
 *
 *   // Optional settings.
 *   // The number of times to retry to connect to the master server before
 *   // failing over.
 *   'retries' => 3,
 *   // The wait time before retries, in microseconds.
 *   'wait_time' => 100000,
 *   // The number of times to multiply the wait time between retries.
 *   'wait_multiplier' => 2,
 *   // An array of transcient errors (from the native driver) on which not to
 *   // failover. It is recommended to ignore at least 1040 and 1203 (Too many
 *   // connections) on MySQL, as retrying on those would be catastrophic for
 *   // the infrastructure.
 *   'transcient_errors' => array(1040, 1203),
 * );
 * @endcode
 */
class DatabaseConnection_failover {
  function __construct(array $connection_options = array()) {
    // Merge in our defaults.
    $connection_options += array(
      'wait_time' => 100000,
      'wait_multiplier' => 2,
      'retries' => 3,
      'transcient_errors' => array(),
    );

    // Load the file of the parent class. This is exactly what
    // Database::openConnection() does.
    $driver = $connection_options['parent_driver'];
    require_once DRUPAL_ROOT . '/includes/database/' . $driver . '/database.inc';

    // All the values in the connection options array are used as default
    // for the servers, except the server array itself.
    $default_options = $connection_options;
    $default_options['driver'] = $default_options['parent_driver'];
    unset($default_options['parent_driver'], $default_options['servers']);

    $candidate_servers = array();
    foreach ($connection_options['servers'] as $server_info) {
      $server_info = $default_options + $server_info;
      $hash = sha1(var_export($server_info, TRUE));
      $candidate_servers[$hash] = $server_info;
    }

    // Get the name of the current master server.
    $agent_class = new ReflectionClass($connection_options['agent'][0]);
    $agent = $agent_class->newInstanceArgs($connection_options['agent'][1]);
    $current_master = $agent->getMaster();

    if ($current_master && isset($candidate_servers[$current_master])) {
      // Place the current master at the begining of the list.
      $candidate_servers = array($current_master => $candidate_servers[$current_master]) + $candidate_servers;
    }

    // Connect to each server in turn.
    foreach ($candidate_servers as $hash => $server_info) {
      $wait = $connection_options['wait_time'];
      for ($i = 0; $i < $connection_options['retries']; $i++) {
        try {
          $connection = @new DatabaseConnection_mysql($server_info);
          $new_master = $hash;
          break 2;
        }
        catch (PDOException $e) {
          // Cannot connect, due to a transcient error, bail out immediately.
          // Retrying on those errors would bring the infrastructure to its knees.
          if (in_array($e->getCode(), $connection_options['transcient_errors'])) {
            throw new Exception('Master server is temporarily unavailable.');
          }
          usleep($wait);
          $wait *= $connection_options['wait_multiplier'];
        }
      }
    }

    if (!$connection) {
      throw new Exception('No servers are available.');
    }

    if ($new_master != $current_master) {
      // Save the new master.
      $agent->setMaster($new_master);
    }

    $this->proxy = $connection;
  }

  /**
   * Transfer all the method calls to the proxy object.
   */
  function __call($method, $arguments) {
    return call_user_func_array(array($this->proxy, $method), $arguments);
  }
}

/**
 * The agent that will be used to get the current master server.
 */
interface DatabaseFailoverAgent {
  /**
   * Get the hostname of the master server.
   */
  function getMaster();

  /**
   * Store the hostname of the master server.
   */
  function setMaster($host);
}

/**
 * An agent that stores the name of the master in a file.
 */
class DatabaseFailoverAgent_fs {
  function __construct($filename) {
    $this->filename = $filename;
  }

  function getMaster() {
    return @file_get_contents($this->filename);
  }

  function setMaster($host) {
    return @file_put_contents($this->filename, $host);
  }
}
